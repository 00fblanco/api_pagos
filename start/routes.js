'use strict'
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    Route.get('/PorRFC/:rfc', 'EmpleadoController.buscaPorRFC');
    Route.get('/Plazas/:EmpleadoId', 'EmpleadoController.buscaPlazas');
    Route.get('/Plazas/:EmpleadoId/:PlazaId', 'EmpleadoController.buscaEmpleadoPlaza');
    Route.get('/BeneficiariosPension/:EmpleadoId', 'EmpleadoController.buscaBeneficiariosPension');
}).prefix('/Empleado');


Route.group(() => {
    Route.get('/map/:splaza', 'PlazaController.buscaMap');
    Route.get('/historia_plaza_ct/:PlazaId', 'PlazaController.buscaHPCT');
    Route.get('/plaza_estatus/:PlazaId', 'PlazaController.buscaPlazaEstatus');
}).prefix('/Plaza');


Route.group(() => {
    //Route.get('/:plaza', 'PagoController.buscaQnas');
    Route.get('/:plaza/:fecha', 'PagoController.buscaPagoPorFechaYPlaza');
    Route.get('/:plaza', 'PagoController.buscarPagos');
    
}).prefix('/Pagos');

Route.group(() => {
    Route.get('/', 'ConceptoController.buscaConceptos');
    
}).prefix('/Concepto');




