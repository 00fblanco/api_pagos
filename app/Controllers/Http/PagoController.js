'use strict'
const Database = use('Database')
const dbFinanzas = Database.connection('mssql_finanzas');
const moment = use('moment')

class PagoController {


    async buscaQnas({ request, response }){
        const plaza = request.params.plaza;

        let data = await dbFinanzas
            .raw('select a.periodo_,a.tipo_nomina,a.no_nomina  from (select * from cabezal union all SELECT * FROM cabezal_2015_2020)  a where a.CLAVE_PLAZA=? order by periodo_ desc', plaza);

            return response.status(200).json(data)
        
    }
    async buscarPagos({ request, response }){
        const plaza = request.params.plaza;

        let data = await dbFinanzas
            .raw("select a.*,FORMAT (A.FECHA_INICIO, 'dd/MM/yyyy') as _FECHA_INICIO,FORMAT (A.FECHA_TERMINO, 'dd/MM/yyyy') as _FECHA_TERMINO  from (select * from cabezal union all SELECT * FROM cabezal_2015_2020)  a where a.CLAVE_PLAZA=?  order by periodo_ desc", [plaza]);
        
        let _dataProm =  data.map(async reg => {
            var detalle = await dbFinanzas
            .raw("select a.* from (select * from detalle union all SELECT * FROM detalle_2015_2020)  a where a.tipo_nomina=? and a.periodo_=? and a.no_nomina=? and a.curp=? and a.clave_plaza=? and a.num_comprobante=? and a.cve_concepto not in ('ISR') order by periodo_ desc", [reg.TIPO_NOMINA,reg.PERIODO_,reg.NO_NOMINA,reg.CURP,reg.CLAVE_PLAZA,reg.NUM_COMPROBANTE]);
            reg.detalle = detalle;
           
            return reg;
        });

        const _data = await Promise.all(_dataProm);
        return response.status(200).json(_data)
        
    }
    async buscaPagoPorFechaYPlaza({ request, response }){
        const plaza = request.params.plaza;
        const fecha = request.params.fecha;

        const _fecha = moment(fecha, 'YYYY-MM-DD');
        /*var dia   = _fecha.format('DD');
        var mes   = _fecha.format('MM');
        var anio   = _fecha.format('YYYY');
        var fecha_inicial = null;
        var fecha_final = null;
        if(dia>=1 & dia <=15){
            fecha_inicial = moment(`${anio}${mes}01`, 'YYYY-MM-DD');
            fecha_final = moment(`${anio}${mes}15`, 'YYYY-MM-DD');
        }else{
            fecha_inicial = moment(`${anio}${mes}16`, 'YYYY-MM-DD');
            fecha_final = _fecha.endOf('month');
        }*/
        let data = await dbFinanzas
            .raw("select a.*,FORMAT (A.FECHA_INICIO, 'dd/MM/yyyy') as _FECHA_INICIO,FORMAT (A.FECHA_TERMINO, 'dd/MM/yyyy') as _FECHA_TERMINO  from (select * from cabezal union all SELECT * FROM cabezal_2015_2020)  a where a.CLAVE_PLAZA=? and a.FECHA_INICIO<=? and a.FECHA_TERMINO>=? order by periodo_ desc", [plaza,fecha,fecha]);

        let _dataProm =  data.map(async reg => {
            var detalle = await dbFinanzas
            .raw("select a.* from (select * from detalle union all SELECT * FROM detalle_2015_2020)  a where a.tipo_nomina=? and a.periodo_=? and a.no_nomina=? and a.curp=? and a.clave_plaza=? and a.num_comprobante=? and a.cve_concepto not in ('ISR') order by periodo_ desc", [reg.TIPO_NOMINA,reg.PERIODO_,reg.NO_NOMINA,reg.CURP,reg.CLAVE_PLAZA,reg.NUM_COMPROBANTE]);
            reg.detalle = detalle;
            
            return reg;
        });

        const _data = await Promise.all(_dataProm);
        return response.status(200).json(_data)
        
    }
}

module.exports = PagoController
