'use strict'
const Database = use('Database')


class ConceptoController {

    async buscaConceptos({ request, response }) {
        let data = await Database.table('NMN.Concepto');

        let percepciones = data.filter((x) => x.PercDed == 'P');
        let deducciones = data.filter((x) => x.PercDed == 'D');
        
        var _data =
        {
            group: [
                { label: 'Percepcciones', cptos: percepciones },
                { label: 'Deducciones', cptos: deducciones }]
        }


        return response.status(200).json(_data)
    }
}

module.exports = ConceptoController
