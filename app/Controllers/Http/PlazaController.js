'use strict'

const database = require("../../../config/database");

const Database = use('Database')
var numeral = require('numeral');


const dbd = Database.connection('mssql_dbd');
class PlazaController {

    async buscaMap({ request, response }){
        const splaza = request.params.splaza;

        let data = await dbd.table('AnaliticoDePlazas')
        .where('CODIGO_PLAZA', splaza);

        return response.status(200).json(data)
    }

    async buscaHPCT({ request, response }){
        const PlazaId = request.params.PlazaId;

        let data = await Database.table('NMN.HistoriaPlazaCentroTrabajo')
            .where('NMN.HistoriaPlazaCentroTrabajo.PlazaId', PlazaId)
            .innerJoin('NMN.CentroTrabajo','NMN.CentroTrabajo.CentroTrabajoId', 'NMN.HistoriaPlazaCentroTrabajo.CentroTrabajoId')
            .select('NMN.CentroTrabajo.EntFedId','NMN.CentroTrabajo.CtClasifId','NMN.CentroTrabajo.CtIdeId','NMN.CentroTrabajo.CtSecuencial','NMN.CentroTrabajo.CtDigitoVer','NMN.CentroTrabajo.UnidadDistChequeId','NMN.HistoriaPlazaCentroTrabajo.QnaIni','NMN.HistoriaPlazaCentroTrabajo.QnaFin', 'NMN.CentroTrabajo.ZonaEconomicaId')
            .orderBy('NMN.HistoriaPlazaCentroTrabajo.QnaFin', 'desc')

        data.map(x => {
            //return $"{ent_fed.ToString("00")}{ct_clasif}{ct_id}{ct_secuencial.ToString("0000")}{ct_digito_ver}";
            x.cct = `${numeral(x.EntFedId).format('00')}${x.CtClasifId}${x.CtIdeId}${numeral(x.CtSecuencial).format('0000')}${x.CtDigitoVer}`;
            return x;
        });
        return response.status(200).json(data)

    }

    async buscaPlazaEstatus({ request, response }){
        const PlazaId = request.params.PlazaId;
        let data = await Database.table('NMN.HistoriaPlazaEstatus')
        .where('PlazaId', PlazaId)
        .orderBy('QnaFin', 'desc')

        return response.status(200).json(data)

    }
}

module.exports = PlazaController
