'use strict'
const Database = use('Database')

const Empleado = use('App/Models/Empleado')
const Persona = use('App/Models/Persona')
const EmpleadoPlaza = use('App/Models/EmpleadoPlaza')
var numeral = require('numeral');


/**
 * Resourceful controller for interacting with empleados
 */
class EmpleadoController {
  async buscaPorRFC({ request, response }) {
    const rfc = request.params.rfc;
    let data = await Persona.query()
      .where('Rfc', 'like', `${rfc}%`)
      .with('Empleado')
      .fetch();

    return response.status(200).json(data)
  }

  async buscaEmpleadoPlaza({request, response}){
    const EmpleadoId = request.params.EmpleadoId;
    const PlazaId = request.params.PlazaId;


    let data = await EmpleadoPlaza.query()
      .where('EmpleadoId', EmpleadoId)
      .where('PlazaId', PlazaId)
      .orderBy('QnaFin', 'desc')
      .with('Plaza')
      .fetch()
    data = data.toJSON();
    return response.status(200).json(data)
  }
  async buscaPlazas({ request, response }) {

    const EmpleadoId = request.params.EmpleadoId;

    let data = await EmpleadoPlaza.query()
      .where('EmpleadoId', EmpleadoId)
      .orderBy('QnaFin', 'desc')
      .with('Plaza')
      .fetch()
    data = data.toJSON();
    //console.log(data);

    // valor unico por plaza distinct 
    var _data = data.filter((elem, index) =>
      data.findIndex(obj => obj.PlazaId === elem.PlazaId) === index
    );

    let pActivas = _data.filter((x) => {
      if (x.BanPago == 1) {
        x.Plaza.pfs = `07${numeral(x.Plaza.UnidadId).format('00')}${numeral(x.Plaza.SubUnidadId).format('00')}${numeral(x.Plaza.Horas).format('00.0')}${x.Plaza.CategoriaPuestoId}${numeral(x.Plaza.ConsPlaza).format('000000')}`;
        return x.Plaza
      }
    });
    let pInactivas = _data.filter((x) => {
      if (x.BanPago == 0) {
        x.Plaza.pfs = `07${numeral(x.Plaza.UnidadId).format('00')}${numeral(x.Plaza.SubUnidadId).format('00')}${numeral(x.Plaza.Horas).format('00.0')}${x.Plaza.CategoriaPuestoId}${numeral(x.Plaza.ConsPlaza).format('000000')}`;
        return x.Plaza
      }
    });
    _data = 
      {
        group: [
          { label: 'Activas', plazas: pActivas },
          { label: 'Inactivas', plazas: pInactivas }],
          plazas: _data.filter(x => {  
                  x.Plaza.pfs = `07${numeral(x.Plaza.UnidadId).format('00')}${numeral(x.Plaza.SubUnidadId).format('00')}${numeral(x.Plaza.Horas).format('00.0')}${x.Plaza.CategoriaPuestoId}${numeral(x.Plaza.ConsPlaza).format('000000')}`;
                  return x;
                }) 
      }
      
    

    return response.status(200).json(_data)
  }


  async buscaBeneficiariosPension({ request, response }) {
    const EmpleadoId = request.params.EmpleadoId;
    let data = await Database.table('NMN.EmpleadoBeneficiario')
      .innerJoin('NMN.Beneficiario', 'EmpleadoBeneficiario.BeneficiarioId', 'NMN.Beneficiario.BeneficiarioId')
      .innerJoin('NMN.Persona', 'NMN.Beneficiario.PersonaId', 'NMN.Persona.PersonaId')
      .where('NMN.EmpleadoBeneficiario.EmpleadoId', EmpleadoId)
      .orderBy('NMN.EmpleadoBeneficiario.QnaFin', 'desc')
      .select('NMN.EmpleadoBeneficiario.NumBeneficiario', 'NMN.EmpleadoBeneficiario.FormaAplicacionId', 'NMN.EmpleadoBeneficiario.Importe', 'NMN.EmpleadoBeneficiario.QnaIni', 'NMN.EmpleadoBeneficiario.QnaFin', 'NMN.Persona.Nombre', 'NMN.Persona.PrimerApellido', 'NMN.Persona.SegundoApellido', 'NMN.Persona.Rfc',)

    return response.status(200).json(data)
  }
}

module.exports = EmpleadoController
