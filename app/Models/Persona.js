'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Persona extends Model {

    static get table () {
        return 'NMN.Persona'
      }

      Empleado(){
        return this.belongsTo('App/Models/Empleado', "PersonaId", "PersonaId");
      }
}

module.exports = Persona
