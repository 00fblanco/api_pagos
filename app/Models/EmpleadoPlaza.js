'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class EmpleadoPlaza extends Model {

    static get table () {
        return 'NMN.EmpleadoPlaza'
    }

    Plaza(){
        return this.belongsTo('App/Models/Plaza', "PlazaId", "PlazaId");
    }
}

module.exports = EmpleadoPlaza
