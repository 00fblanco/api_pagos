'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Plaza extends Model {
    static get table () {
        return 'NMN.Plaza'
    }
}

module.exports = Plaza
